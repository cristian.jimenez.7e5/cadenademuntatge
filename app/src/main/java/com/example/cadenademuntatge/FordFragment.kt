package com.example.cadenademuntatge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cadenademuntatge.databinding.FragmentFordBinding
import kotlinx.coroutines.*

class FordFragment : Fragment() {

    lateinit var binding: FragmentFordBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.startWorking.setOnClickListener {

            CoroutineScope(Dispatchers.Main).launch {
                val firstOperator = async { initFirstOperator() }
                val secondOperator = async { initSecondOperator(firstOperator.await()) }
                val thirdOperator = async { initThirdOperator() }
                async { initFourthOperator(secondOperator.await(), thirdOperator.await()) }
            }
        }
    }


    private suspend fun initFirstOperator(): Int{
        var totalUnits = 0
        repeat(5){
            totalUnits += getRandomUnits()
        }
        binding.firstOperatorText.text = "Work completed ($totalUnits units)"
        return totalUnits
    }

    private suspend fun initSecondOperator(numberOfUnits: Int): Int{
        val groupedUnits = groupUnits(numberOfUnits)
        binding.secondOperatorText.text = "Work completed ($groupedUnits packages)"
        return groupedUnits
    }

    private suspend fun initThirdOperator(): Int{
        val containerPositions = getRandomContainerPosition()
        binding.thirdOperatorText.text = "Work completed ($containerPositions positions)"
        return containerPositions
    }

    private fun initFourthOperator(numberOfPackages: Int, numberOfPositions: Int){
        val textToShow = fitUnitsInContainers(numberOfPackages, numberOfPositions)
        binding.fourthOperatorText.text = textToShow
    }

    private suspend fun getRandomUnits(): Int{
        delay(3000)
        return (3..21).random()
    }

    private suspend fun groupUnits(numberOfUnits: Int): Int{
        delay(1000)
        return numberOfUnits/4
    }

    private suspend fun getRandomContainerPosition(): Int{
        delay((10000..20000).random().toLong())
        return (2..4).random()
    }

    private fun fitUnitsInContainers(numberOfPackages: Int, positions: Int): String {
        if (numberOfPackages % positions == 0) {
            return "Han cabut els $numberOfPackages paquets i no n'ha sobrat cap."
        } else {
            val extraPackages = numberOfPackages % positions
            return "Han sobrat $extraPackages paquets. "
        }
    }
}